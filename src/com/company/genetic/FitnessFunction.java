package com.company.genetic;

public interface FitnessFunction {
	public double calculateFitness(boolean[] individual);
}
